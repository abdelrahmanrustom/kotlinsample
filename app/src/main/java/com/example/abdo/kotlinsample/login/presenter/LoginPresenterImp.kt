package com.example.abdo.kotlinsample.login.presenter

import android.util.Log
import com.example.abdo.kotlinsample.data.source.DataRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


import javax.inject.Inject

/**
 * Created by Abdo on 28/08/2018.
 */
class LoginPresenterImp @Inject constructor(dataRepository : DataRepository, view : LoginContract.View ) : LoginContract.Presenter{
    var mDataRepository = dataRepository
    var mView = view
    @Inject
    fun setupListeners(){
        mView.setPresenter(this)
    }

    override fun signIn(email: String, password: String) {
       // Log.d("LoginPresenterImp","before "+ email + " password "+password)
        mDataRepository.login(email, password, "1234").subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(
                {
                    result ->
                        if(result.status.equals("1")){
                            mView.showError(result.errors!!)
                        } else {
                            mView.signInSuccess()
                        }
                },
                { error -> //Log.d("LoginPresenterImp","Error")
                    mView.showError(error.message!!)
                }
        )
    }

}