package com.example.abdo.kotlinsample.login.di

import com.example.abdo.kotlinsample.login.view.MainActivityFragment
import com.example.abdo.kotlinsample.data.source.DataRepositoryComponent
import com.example.abdo.kotlinsample.utils.FragmentScoped
import dagger.Component

/**
 * Created by Abdo on 28/08/2018.
 */

@FragmentScoped
@Component(dependencies = arrayOf(DataRepositoryComponent::class), modules = arrayOf(LoginModule::class))
interface LoginComponent {
    fun inject(fragment: MainActivityFragment)
}