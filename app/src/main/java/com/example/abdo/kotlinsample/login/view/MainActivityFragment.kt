package com.example.abdo.kotlinsample.login.view

import android.support.v4.app.Fragment
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.abdo.kotlinsample.KotlinApp
import com.example.abdo.kotlinsample.R
import com.example.abdo.kotlinsample.login.di.DaggerLoginComponent
import com.example.abdo.kotlinsample.login.di.LoginModule
import com.example.abdo.kotlinsample.login.presenter.LoginContract
import com.example.abdo.kotlinsample.utils.NavigationManager.Companion.goToMainActivity
import kotlinx.android.synthetic.main.fragment_main.*
import javax.inject.Inject

/**
 * A placeholder fragment containing a simple view.
 */
class MainActivityFragment : Fragment(), LoginContract.View {
    @Inject
    lateinit var mLoginPresenter: LoginContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DaggerLoginComponent.builder()
                .dataRepositoryComponent((activity.application as KotlinApp).mDataRepositoryComponent)
                .loginModule(LoginModule(this))
                .build().inject(this)

    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return  inflater.inflate(R.layout.fragment_main, container, false);
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        button.setOnClickListener {
           // Toast.makeText(this@MainActivityFragment.context, "email: ${email.text} pass: ${password.text}", Toast.LENGTH_LONG).show()
            mLoginPresenter?.signIn(email.text.toString(), password.text.toString())
        }
        super.onViewCreated(view, savedInstanceState)
    }

    override fun setPresenter(presenter: LoginContract.Presenter) {
        mLoginPresenter = presenter
    }

    override fun showError() {
        Toast.makeText(this@MainActivityFragment.context, "Error", Toast.LENGTH_LONG).show()
        //Log.d("MainActivityFragment","Error")
    }

    override fun showError(message: String) {
        Toast.makeText(this@MainActivityFragment.context, message, Toast.LENGTH_LONG).show()
        //Log.d("MainActivityFragment","Error "+message)
    }

    override fun signInSuccess() {
        //Log.d("MainActivityFragment","signInSuccess ")
        Toast.makeText(this@MainActivityFragment.context, "Success", Toast.LENGTH_LONG).show()
        goToMainActivity(context, null)
    }
}

