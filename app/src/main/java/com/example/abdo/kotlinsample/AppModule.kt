package com.example.abdo.kotlinsample

import android.content.Context
import dagger.Module
import dagger.Provides

/**
 * Created by Abdo on 28/08/2018.
 */
@Module
class AppModule(mApplication: Context) {
    val mContext: Context = mApplication

    @Provides
    fun provideApplication(): Context{
        return mContext
    }
}