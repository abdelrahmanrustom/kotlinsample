package com.example.abdo.kotlinsample.data.source

import com.example.abdo.kotlinsample.data.source.remote.RemoteDataSource
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Abdo on 28/08/2018.
 */
@Module
class DataRepositoryModule {
    @Singleton
    @Provides
    fun provideDataRepository(): DataSource {
        return RemoteDataSource()
    }
}