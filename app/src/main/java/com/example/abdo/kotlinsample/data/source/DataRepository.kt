package com.example.abdo.kotlinsample.data.source

import android.content.Context
import com.example.abdo.kotlinsample.data.source.remote.model.User
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by Abdo on 28/08/2018.
 */
@Singleton
class DataRepository @Inject constructor(dataSource: DataSource, context: Context): DataSource {
    var mRemoteDataSource: DataSource = dataSource

    override fun login(email: String, password: String, device_IMEI: String): Observable<User> {
        return mRemoteDataSource.login(email, password, device_IMEI)
    }

}