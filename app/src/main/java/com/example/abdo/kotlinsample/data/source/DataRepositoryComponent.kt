package com.example.abdo.kotlinsample.data.source

import com.example.abdo.kotlinsample.AppModule
import dagger.Component
import javax.inject.Singleton

/**
 * Created by Abdo on 28/08/2018.
 */
@Singleton
@Component(modules = arrayOf(AppModule::class, DataRepositoryModule::class))
interface DataRepositoryComponent {
    fun getDataRepository(): DataRepository
}