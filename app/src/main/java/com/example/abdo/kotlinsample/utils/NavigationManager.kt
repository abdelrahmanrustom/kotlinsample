package com.example.abdo.kotlinsample.utils

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.example.abdo.kotlinsample.home.HomeActivity

/**
 * Created by Abdo on 29/08/2018.
 */
open class NavigationManager {
    companion object {
        fun goToMainActivity(context: Context, bundle: Bundle?) {
            val intent = Intent(context, HomeActivity::class.java)
            if (bundle != null)
                intent.putExtras(bundle)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            context.startActivity(intent)
        }
    }
}