package com.example.abdo.kotlinsample.utils

import java.lang.annotation.Documented
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import javax.inject.Scope

/**
 * Created by Abdo on 28/08/2018.
 */

@Documented
@Scope
@Retention(RetentionPolicy.RUNTIME)
annotation class FragmentScoped