package com.example.abdo.kotlinsample.login.presenter

import com.example.abdo.kotlinsample.IBaseView

/**
 * Created by Abdo on 28/08/2018.
 */
interface LoginContract {
    interface View : IBaseView<Presenter> {
        fun showError()
        fun showError(message: String)
        fun signInSuccess()
    }
    interface Presenter {
        fun signIn(email: String, password: String)
    }
}