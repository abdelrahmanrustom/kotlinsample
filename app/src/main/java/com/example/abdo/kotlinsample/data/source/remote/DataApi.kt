package com.example.abdo.kotlinsample.data.source.remote

import com.example.abdo.kotlinsample.data.source.remote.model.User
import io.reactivex.Observable
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

/**
 * Created by Abdo on 28/08/2018.
 */
interface DataApi {
    @FormUrlEncoded
    @POST("sign-in")
    fun login(@Field("email") email: String,
              @Field("password")password: String,
              @Field("device_IMEI")device_IMEI: String) : Observable<User>
}