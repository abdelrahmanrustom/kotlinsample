package com.example.abdo.kotlinsample.login.di

import com.example.abdo.kotlinsample.login.presenter.LoginContract
import com.example.abdo.kotlinsample.login.presenter.LoginPresenterImp
import dagger.Module
import dagger.Provides

/**
 * Created by Abdo on 28/08/2018.
 */
@Module
class LoginModule( view: LoginContract.View) {
    var mView = view

    @Provides
    fun provideLoginView() :LoginContract.View {
        return mView
    }

    @Provides
    fun registerAccountPresenter(impl: LoginPresenterImp) :LoginContract.Presenter {
        return impl
    }
}