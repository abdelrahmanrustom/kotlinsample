package com.example.abdo.kotlinsample

/**
 * Created by Abdo on 28/08/2018.
 */
interface IBaseView<A> {
    fun setPresenter(presenter: A)
}