package com.example.abdo.kotlinsample.data.source.remote.model

import com.google.gson.annotations.SerializedName

/**
 * Created by Abdo on 28/08/2018.
 */
data class User(
                @SerializedName("auth_key")
                var auth_key :String? =""
) :BaseResponse()