package com.example.abdo.kotlinsample.data.source.remote.model

import com.google.gson.annotations.SerializedName

/**
 * Created by Abdo on 28/08/2018.
 */
open class BaseResponse(@SerializedName("status")
                        var status :String? ="",
                        @SerializedName("errors")
                        var errors :String? =""
)