package com.example.abdo.kotlinsample.data.source.remote

import com.example.abdo.kotlinsample.data.source.DataSource
import com.example.abdo.kotlinsample.data.source.remote.model.User
import io.reactivex.Observable

/**
 * Created by Abdo on 28/08/2018.
 */
class RemoteDataSource : BaseRemoteDataSource() ,DataSource {
    override fun login(email: String, password: String, device_IMEI: String): Observable<User> {
        return createService(DataApi::class.java).login(email, password, device_IMEI)
    }

}