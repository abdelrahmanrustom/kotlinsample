package com.example.abdo.kotlinsample.data.source.remote

import com.example.abdo.kotlinsample.BuildConfig
import com.example.abdo.kotlinsample.utils.Constants.Companion.PRE_URL
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

import java.util.concurrent.TimeUnit

/**
 * Created by Abdo on 28/08/2018.
 */
open class BaseRemoteDataSource {
    val mRetrofit: Retrofit by lazy {
            Retrofit.Builder().baseUrl(PRE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .addConverterFactory(GsonConverterFactory.create())
            .client(mOkHttpClient).build() }

    val mOkHttpClient: OkHttpClient by lazy{
        val builder = OkHttpClient.Builder()
        if (BuildConfig.DEBUG){
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            builder.addInterceptor(logging)
        }
        builder.readTimeout(5, TimeUnit.MINUTES).connectTimeout(5, TimeUnit.MINUTES).build()
    }

    fun <T> createService(service: Class<T>):  T{
        return mRetrofit.create(service)
    }

}