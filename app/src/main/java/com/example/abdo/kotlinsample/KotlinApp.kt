package com.example.abdo.kotlinsample

import android.app.Application
import android.util.Log
import com.example.abdo.kotlinsample.data.source.DaggerDataRepositoryComponent
import com.example.abdo.kotlinsample.data.source.DataRepositoryComponent
import com.example.abdo.kotlinsample.data.source.DataRepositoryModule

/**
 * Created by Abdo on 28/08/2018.
 */
open class KotlinApp: Application() {
    lateinit var mDataRepositoryComponent: DataRepositoryComponent

    override fun onCreate() {
        super.onCreate()
        //Log.d("KotlinApp", "OnCreate KotlinApp")
        mDataRepositoryComponent = DaggerDataRepositoryComponent.builder()
                .appModule(AppModule(applicationContext)).dataRepositoryModule(DataRepositoryModule()).build();
    }
}