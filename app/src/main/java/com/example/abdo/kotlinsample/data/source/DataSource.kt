package com.example.abdo.kotlinsample.data.source

import com.example.abdo.kotlinsample.data.source.remote.model.User
import io.reactivex.Observable

/**
 * Created by Abdo on 28/08/2018.
 */

interface DataSource {
    fun login( email: String, password: String, device_IMEI: String): Observable<User>
}